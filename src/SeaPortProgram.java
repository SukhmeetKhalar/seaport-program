// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar
//aaaaaaaaaaaaa

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SeaPortProgram {
   private static World world;

   //Main Method
   public static void main(String[] args) {
       Scanner scan;
       world = new World();
       try {
           scan = new Scanner(new File(simulateFile()));
           world.readFile(scan);
           scan = null;
           System.gc();
           JDisplay jDisplay = new JDisplay(world.toString());
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       }
   }

   private static String simulateFile() {
       String file = null;
       JFileChooser jFileChooser = new JFileChooser(".");
       jFileChooser.setDialogTitle("Please select a data file");
       jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
       if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
           file = jFileChooser.getSelectedFile().toString();
       } else {
           System.exit(0);
       }
       return file;
   }

   private static class JDisplay extends JFrame {
	   //Auto generated serialVersionUID
	   private static final long serialVersionUID = -5267028481821685943L;
	   //declare the variables
       String str;
       JTextArea text;
       JComboBox <String> sortBox;
       JComboBox <String> combo;
       JTextField textField;
       JButton data;
       JButton sortByNameButton;
       JScrollPane scroll;
       JTree jTree;
       JScrollPane scrollPane;
       private JDisplay(String str) {
           this.str = str;
           createAndShowGUI();
       }

       private void addComponents(Container contentPane) {
           initComponentsAndSetFeatures();
           addActionListeners();
           setLayoutForDisplay(contentPane);
       }

       private void addActionListeners() {
           sortBox.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent e) {
                   int index = sortBox.getSelectedIndex();
                   String sorted = sortShipsInQue(index);
                   if (index != 0) {  //&& !sorted.matches(str) got rid of the error. Why?
                       text.setText(sorted);
                       data.setEnabled(true);
                   }
               }
           });
           combo.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent e) {
                   textField.setEditable(combo.getSelectedIndex() != 0);
               }
           });
           textField.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent e) {
                   // Remove all redundant spaces
                   String searchStr = textField.getText().replaceAll("[\\s\\t]+$", "").replaceAll("^[\\s\\t]+", "");
                   searchStr = searchStr.replaceAll("(\\s+){2,}", "");
                   String result = search(searchStr);
                   if (result != null && result.length() > 0) {
                       text.setText(result);
                   } else {
                       text.setText("No result found!");
                   }
                   data.setEnabled(result == null);
               }
           });
           data.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent e) {
                   text.setText(str);
                   data.setEnabled(false);
               }
           });
           sortByNameButton.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent e) {
                   text.setText(world.sortByShipName());
                   data.setEnabled(true);
               }
           });
       }

       private String search(String stringSearch) {
           switch (combo.getSelectedIndex()) {
               case 1:
                   return world.searchIndex(stringSearch);
               case 2:
                   return world.searchName(stringSearch);
               case 3:
                   return world.searchSkill(stringSearch);
               default:
                   return "";
           }
       }

       private String sortShipsInQue(int index) {
           switch (index) {
               case 1:
                   return world.sortByShipWeight();
               case 2:
                   return world.sortByShipLength();
               case 3:
                   return world.sortByWidth();
               case 4:
                   return world.sortByShipDraft();
               default:
                   return "";
           }
       }

       private void setLayoutForDisplay(Container contentPane) {
           JSplitPane jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scroll, scrollPane);
           JPanel containerForJobs = new JPanel();
           JPanel containerForResources = new JPanel();
           containerForJobs.setLayout(new BoxLayout(containerForJobs, BoxLayout.Y_AXIS));
           containerForResources.setLayout(new BoxLayout(containerForResources, BoxLayout.Y_AXIS));
           boolean hasJob = false;
           boolean hasResource = false;
           for (SeaPort seaPort : world.getPorts().values()) {
               containerForResources.add(seaPort.getContainerPanel());
               if (!hasResource)
                   hasResource = seaPort.persons.size() > 0;
               for (Ship ship : seaPort.ships) {
                   for (Job job : ship.getShips()) {
                       containerForJobs.add(job.getPanel());
                       hasJob = true;
                   }
               }
           }
           JScrollPane sPaneForJobsContainer = new JScrollPane(new JLabel("No jobs!".toUpperCase(), SwingConstants.CENTER));
           if (hasJob)
               sPaneForJobsContainer = new JScrollPane(containerForJobs);
           JScrollPane sPaneForResourcesContainer = new JScrollPane(new JLabel("No resources!".toUpperCase(), SwingConstants.CENTER));
           if (hasResource)
               sPaneForResourcesContainer = new JScrollPane(containerForResources);
           sPaneForJobsContainer.setMaximumSize(new Dimension(5660, 800));
           sPaneForResourcesContainer.setMaximumSize(new Dimension(5660, 200));
           GroupLayout layout = new GroupLayout(contentPane);
           contentPane.setLayout(layout);
           layout.setAutoCreateGaps(true);
           layout.setAutoCreateContainerGaps(true);
           layout.setHorizontalGroup(layout.createSequentialGroup()
                   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                           .addGroup(layout.createSequentialGroup()
                                   .addComponent(combo)
                                   .addComponent(textField))
                           .addComponent(sortBox)
                           .addComponent(jSplitPane)
                           .addGroup(layout.createSequentialGroup()
                                   .addComponent(data)
                                   .addComponent(sortByNameButton))
                           .addComponent(sPaneForResourcesContainer))
                   .addComponent(sPaneForJobsContainer));
           layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                   .addGroup(layout.createSequentialGroup()
                           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                   .addComponent(combo)
                                   .addComponent(textField))
                           .addComponent(sortBox)
                           .addComponent(jSplitPane)
                           .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                   .addComponent(data)
                                   .addComponent(sortByNameButton))
                           .addComponent(sPaneForResourcesContainer))
                   .addComponent(sPaneForJobsContainer));
       }

       private void initComponentsAndSetFeatures() {
           String[] sortOptions = {
                    "Select a feature to sort ships in que",
                    "weight",
                    "length",
                    "width",
                    "draft"
            };
           String[] searchOptions = {
                    "Select a feature to search",
                    "index",
                    "name",
                    "skill"
            };
           text = new JTextArea(str, 20, 60);
           sortBox = new JComboBox <String> (sortOptions);
           combo = new JComboBox <String> (searchOptions);
           textField = new JTextField();
           data = new JButton("Reload Data");
           sortByNameButton = new JButton("Sort All Things");
           textField.setPreferredSize(new Dimension(120, 25));
           textField.setEditable(false);
           sortBox.setSelectedIndex(0);
           sortBox.setMaximumSize(new Dimension(5660, 25));
           combo.setSelectedIndex(0);
           text.setFont(new java.awt.Font("Monospaced", 0, 12));
           text.setEditable(false);
           text.setLineWrap(true);
           scroll = new JScrollPane(text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
           data.setEnabled(false);
           initTree();
       }

       private void initTree() {
           DefaultMutableTreeNode top = new DefaultMutableTreeNode("World");
           createNodes(top);
           jTree = new JTree(top);
           scrollPane = new JScrollPane(jTree);
       }

       private void createNodes(DefaultMutableTreeNode top) {
           DefaultMutableTreeNode portNode;
           for (SeaPort seaPort : world.getPorts().values()) {
               portNode = createThingNode(seaPort);
               DefaultMutableTreeNode node = new DefaultMutableTreeNode("Docks");
               for (Dock dock : seaPort.docks) {
                   DefaultMutableTreeNode dockNode = createThingNode(dock);
                   if (dock.getShip() != null) {
                       DefaultMutableTreeNode shipNode = createThingNode(dock.getShip());
                       for (Job job : dock.getShip().getShips()) {
                           shipNode.add(createThingNode(job));
                       }
                       dockNode.add(shipNode);
                   }
                   node.add(dockNode);
                   portNode.add(node);
               }
               node = new DefaultMutableTreeNode("Ships in Que");
               for (Ship ship : seaPort.waitingShipsInQue) {
                   DefaultMutableTreeNode shipNode = createThingNode(ship);
                   for (Job job : ship.getShips()) {
                       shipNode.add(createThingNode(job));
                   }
                   node.add(shipNode);
               }
               portNode.add(node);
               node = new DefaultMutableTreeNode("All Ships");
               for (Ship ship : seaPort.ships) {
                   DefaultMutableTreeNode shipNode = createThingNode(ship);
                   for (Job job : ship.getShips()) {
                       shipNode.add(createThingNode(job));
                   }
                   node.add(shipNode);
               }
               portNode.add(node);
               node = new DefaultMutableTreeNode("People");
               for (Person person : seaPort.persons) {
                   DefaultMutableTreeNode personNode = createThingNode(person);
                   node.add(personNode);
               }
               portNode.add(node);
               top.add(portNode);
           }
       }

       private DefaultMutableTreeNode createThingNode(Thing thing) {
           return new DefaultMutableTreeNode(thing.getIndex() + " " + thing.getName());
       }

       private void createAndShowGUI() {
           JFrame frame = new JFrame("Sea Port Simulation");
           frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           addComponents(frame.getContentPane());
           frame.setPreferredSize(new Dimension(1300, 800));
           frame.pack();
           frame.setVisible(true);
           World.setValue(true); // world is set!
       }
   }
}