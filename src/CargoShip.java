import java.util.Scanner;

//define the class CargoShip that extends Ship

public class CargoShip extends Ship {

    private double cargoValue, cargoVolume, cargoWeight;

    public CargoShip(Scanner sc) {
        super(sc);
        if (sc.hasNextDouble())
            cargoValue = sc.nextDouble();
        if (sc.hasNextDouble())
            cargoVolume = sc.nextDouble();
        if (sc.hasNextDouble())
            cargoWeight = sc.nextDouble();
    }

    //Getters
    public double getCargoValue() {
        return cargoValue;
    }

    public double getCargoVolume() {
        return cargoVolume;
    }

    public double getCargoWeight() {
        return cargoWeight;
    }

    //Setters
    public void setCargoValue(double cargoValue) {
        this.cargoValue = cargoValue;
    }

    public void setCargoVolume(double cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    public void setCargoWeight(double cargoWeight) {
        this.cargoWeight = cargoWeight;
    }
}

