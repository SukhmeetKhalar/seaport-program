// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar

import java.util.ArrayList;
import java.util.Scanner;

public class Dock extends Thing {
    private Ship ship;
    public Dock(Scanner sc) {
        super(sc);
    }
    public void setShip(Ship ship) {
        synchronized(this) {
            this.ship = ship;
            if (ship != null) {
                this.ship.thingObject = this;
            }
        }
    }
    public Ship getShip() {
        return ship;
    }

    public void leaveShipFromDock() {
        setShip(null);
        checkDocksAtDock();
    }

    private void requestForNewShip() {
        Ship ship = ((SeaPort) thingObject).getShipFromQue();
        if (ship != null)
            setShip(ship);
    }

    //TODO: Rename and refactor this to checkShipAtDock()
    public void checkDocksAtDock() {
        if (ship == null || ship.getShips().size() == 0)
            requestForNewShip();
    }

    boolean requestPersonnel(ArrayList < String > requirements) {
        return ((SeaPort) thingObject).requestPersonnel(requirements);
    }

    public ArrayList < Person > requestWorkers(ArrayList < String > requirements, Job job) {
        return ((SeaPort) thingObject).requestWorkers(requirements, job);
    }

    public void releaseWorkers(ArrayList < Person > workers) {
        ((SeaPort) thingObject).releaseWorkers(workers);
    }

    public String toString() {
        if (ship != null)
            return "\n Dock: " + super.toString() + "\n " + ship.toString();
        else
            return "\n Dock: " + super.toString();
    }
}