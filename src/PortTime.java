public class PortTime {
    private int time;

    public PortTime(){
        this.time = 0;
    }
    //Getter
    public int getTime() {
        return time;
    }
    //Setter
    public void setTime(int time) {
        this.time = time;
    }
}
