// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar

import java.util.*;

public class World extends Thing {

    private HashMap < Integer, SeaPort > ports;
    private PortTime time;
    private static boolean value = false;

    public World() {
        ports = new HashMap < Integer,SeaPort> ();
    }

    public void readFile(Scanner scan) {
        HashMap < Integer, Dock > dockHashMap = new HashMap < Integer, Dock > ();
        HashMap < Integer, Ship > shipHashMap = new HashMap < Integer, Ship > ();
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            line = line.replaceAll("^\\s+", "");
            if (line.length() > 0 && line.charAt(0) != '/')
            {
                Scanner sc = new Scanner(line);
                if (!sc.hasNext()) {
                	sc.close();
                	return;
                    
                }
                String word = sc.next().toLowerCase();

                //TODO: Change to swithc statement
                switch(word){
                	case "port":
                		addPort(sc);
                		break;
                	case "dock":
                		addDock(sc, dockHashMap);
                		break;
                	case "pship":
                		addPassengerShip(sc, dockHashMap, shipHashMap);
                		break;
                	case "cship":
                		addCargoShip(sc, dockHashMap, shipHashMap);
                		break;
                	case "person":
                		addPerson(sc);
                		break;
                	case "job":
                		addJob(sc, shipHashMap);
                		break;
                	default:
                		sc.close();
                		break;
                }             
                sc.close();
            }
        }
    }

    private void addJob(Scanner sc, HashMap < Integer, Ship > shipHashMap) {
        Job job = new Job(sc);
        if (shipHashMap.get(job.getParent()) != null)
        {
            job.setThingObject(shipHashMap);
            shipHashMap.get(job.getParent()).getShips().add(job);
        }
    }

    private void addPerson(Scanner sc) {
        Person person = new Person(sc);
        ports.get(person.getParent()).addPerson(person);
    }

    private void addCargoShip(Scanner sc, HashMap < Integer, Dock > dockHashMap, HashMap < Integer, Ship > shipHashMap) {
        CargoShip cargoShip = new CargoShip(sc);
        shipHashMap.put(cargoShip.getIndex(), cargoShip);
        assignShip(cargoShip, dockHashMap);
    }

    private void addPassengerShip(Scanner sc, HashMap < Integer, Dock > dockHashMap, HashMap < Integer, Ship > shipHashMap) {
        PassengerShip passengerShip = new PassengerShip(sc);
        shipHashMap.put(passengerShip.getIndex(), passengerShip);
        assignShip(passengerShip, dockHashMap);
    }

    private void addDock(Scanner sc, HashMap < Integer, Dock > dockHashMap)
    {
        Dock dock = new Dock(sc);
        dock.setThingObject(ports);
        dockHashMap.put(dock.getIndex(), dock);
        // Add dock to port
        ports.get(dock.getParent()).addDock(dock);
    }

    private void addPort(Scanner sc) {
        SeaPort seaPort = new SeaPort(sc);
        ports.put(seaPort.getIndex(), seaPort);
    }

    private void assignShip(Ship ship, HashMap < Integer, Dock > dockHashMap) {
        Dock dock = dockHashMap.get(ship.getParent());
        if (dock == null) {
            ship.setThingObject(ports);
            ports.get(ship.getParent()).addShip(ship);
            ports.get(ship.getParent()).addShipToQue(ship);
            return;
        }
        if (dock.getShip() != null)
            ports.get(ship.getParent()).addShipToQue(ship);
        else
            dock.setShip(ship);
        ports.get(dock.getParent()).addShip(ship);
    }

    public String sortByShipName() {
        List < SeaPort > list = new ArrayList < SeaPort > ();
        String result = "";
        for (SeaPort seaPort: ports.values()) {
            seaPort.sortAllListsByName();
            list.add(seaPort);
        }
        Collections.sort(list);
        for (SeaPort seaPort: list) {
            result += "Port: ";
            result += seaPort.getName() + "\n Ships:\n ";
            for (Ship ship: seaPort.ships) {
                result += ship.getName() + "\n ";
                result += " Jobs:\n ";
                for (Job job: ship.getShips()) {
                    result += job.getName() + "\n ";
                }
                result += "\n ";
            }
            result += "\n Docks:\n ";
            for (Dock dock: seaPort.docks) {
                result += dock.getName() + "\n ";
            }
            result += "\n People:\n ";
            for (Person person: seaPort.persons) {
                result += person.getName() + "\n ";
            }
        }
        return result;
    }

    public String sortByShipWeight() {
        String result = "";
        for (SeaPort seaPort: ports.values()) {
            seaPort.sortByWeight();
            result += "Port: ";
            result += seaPort.getName() + "\n Ships:\n ";
            for (Ship ship: seaPort.waitingShipsInQue) {
                result += ship.getName() + ": " + ship.getWeight() + "\n ";
            }
            result += "\n";
        }
        return result;
    }

    public String sortByShipLength() {
        String result = "";
        for (SeaPort seaPort: ports.values()) {
            seaPort.sortByLength();
            result += "Port: ";
            result += seaPort.getName() + "\n Ships:\n ";
            for (Ship ship: seaPort.waitingShipsInQue) {
                result += ship.getName() + ": " + ship.getLength() + "\n ";
            }
            result += "\n";
        }
        return result;
    }

    public String sortByWidth() {
        String result = "";
        for (SeaPort seaPort: ports.values()) {
            seaPort.sortByWidth();
            result += "Port: ";
            result += seaPort.getName() + "\n Ships:\n ";
            for (Ship ship: seaPort.waitingShipsInQue) {
                result += ship.getName() + ": " + ship.getLength() + "\n ";
            }
            result += "\n";
        }
        return result;
    }

    public String sortByShipDraft() {
        String result = "";
        for (SeaPort seaPort: ports.values()) {
            seaPort.sortByDraft();
            result += "Port: ";
            result += seaPort.getName() + "\n Ships:\n ";
            for (Ship ship: seaPort.waitingShipsInQue) {
                result += ship.getName() + ": " + ship.getDraft() + "\n ";
            }
            result += "\n";
        }
        return result;
    }

    private Dock getDockByIndex(int index) {
        for (SeaPort port: ports.values())
            for (Dock dock: port.docks)
                if (dock.getIndex() == index)
                    return dock;
        return null;
    }

    private Ship getShipByIndex(int index) {
        for (SeaPort port: ports.values())
            for (Ship ms: port.ships)
                if (ms.getIndex() == index)
                    return ms;
        return null;
    }

    private Person getPersonByIndex(int index) {
        for (SeaPort port: ports.values()) {
            for (Person person: port.persons) {
                if (person.getIndex() == index)
                    return person;
            }
        }
        return null;
    }

    public String searchIndex(String search) {
        int index;
        try {
            index = Integer.parseInt(search);
        } catch (NumberFormatException e) {
            return "That is not a number!";
        }
        String result = "";
        result += (getDockByIndex(index) != null) ? getDockByIndex(index).toString() : "";
        result += (getShipByIndex(index) != null) ? getShipByIndex(index).toString() : "";
        result += (getPersonByIndex(index) != null) ? getPersonByIndex(index).toString() : "";
        result += (ports.get(index) != null) ? ports.get(index).toString() : "";
        return result;
    }

    public String searchName(String search) {
        String result = "";
        for (SeaPort port: ports.values()) {
            for (Dock dock: port.docks) {
                if (dock.getName().toLowerCase().matches(search.toLowerCase()))
                    result += dock.toString();
            }
            for (Ship ship: port.ships) {
                if (ship.getName().toLowerCase().matches(search.toLowerCase()))
                    result += ship.toString();
            }
            for (Person person: port.persons) {
                if (person.getName().toLowerCase().matches(search.toLowerCase()))
                    result += person.toString();
            }
            if (port.getName().toLowerCase().matches(search.toLowerCase()))
                result += port.toString();
        }
        return result;
    }

    public String searchSkill(String search) {
        String result = "";
        for (SeaPort port: ports.values()) {
            for (Person person: port.persons) {
                if (person.getSkill().toLowerCase().matches(search.toLowerCase()))
                    result += person.toString();
            }
        }
        return result;
    }


    //Getters
    public HashMap<Integer, SeaPort> getPorts() {
        return ports;
    }

    public PortTime getTime() {
        return time;
    }

    public static boolean getValue() {
        return value;
    }

    //Setters
    public static void setValue(boolean newVal) {
        value = newVal;
    }


    //definition of the method toString()
    public String toString() {
        String result = "The world: ";
        for (SeaPort sp: ports.values())
            result += sp.toString();
        return result;
    }
}