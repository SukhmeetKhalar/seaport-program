import java.util.Scanner;

public class Person extends Thing {

    private String skill;
    boolean available = true;

    public Person(Scanner sc) {
        super(sc);
        if (sc.hasNext())
            skill = sc.next();
    }

    public String getSkill() {
        return skill;
    }

    synchronized public Person hirePerson() {
        if (available) {
            available = false;
            return this;
        }
        return null;
    }

    synchronized public void release() {
        if (!available)
            available = true;
    }

    public boolean hasRequiredSkill(String requirement) {
        return requirement.toLowerCase().matches(skill.toLowerCase());
    }
    public String toString() {
        return " Person: " + super.toString() + " " + skill;
    }
}