// File: Dock.java
// Date: July 2, 2018
// Author: Sukhmeet Khalar

import java.util.Scanner;

public class PassengerShip extends Ship{
    private int numOfOccupiedRooms, numOfPassengers, numOfRooms;
    public PassengerShip(Scanner input){
        super(input);
        if(input.hasNextInt())
            numOfPassengers = input.nextInt();
        if(input.hasNextInt())
            numOfRooms = input.nextInt();
        if(input.hasNextInt())
            numOfOccupiedRooms = input.nextInt();
    }

    public int getNumOfOccupiedRooms() {
        return numOfOccupiedRooms;
    }

    public int getNumOfPassengers() {
        return numOfPassengers;
    }

    public int getNumOfRooms() {
        return numOfRooms;
    }

    public void setNumOfOccupiedRooms(int numOfOccupiedRooms) {
        this.numOfOccupiedRooms = numOfOccupiedRooms;
    }

    public void setNumOfPassengers(int numOfPassengers) {
        this.numOfPassengers = numOfPassengers;
    }

    public void setNumOfRooms(int numOfRooms) {
        this.numOfRooms = numOfRooms;
    }
}
