// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Job extends Thing implements Runnable {

    private ArrayList<String> requirements = new ArrayList<String>();
    private JPanel panel;
    private long duration;
    private JProgressBar bar = new JProgressBar();
    private boolean flag = true, killFlag = false;
    private JButton stop = new JButton("Stop");
    private JButton cancel = new JButton("Cancel");
    private Status status = Status.WAITING;
    private Job removedJob;
    private Ship parentShip;

    enum Status {RUNNING, SUSPENDED, WAITING, DONE, CANCELLED}

    //constructor
    public Job(Scanner sc) {
        super(sc);
        duration = (long) sc.nextDouble();
        while (sc.hasNext()) {
            String requirement = sc.next();
            if (requirement != null && requirement.length() > 0)
                requirements.add(requirement);
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
	public void setThingObject(HashMap thinkObject) {
        if (thinkObject.get(getParent()) != null) {
            this.thingObject = (Thing) thinkObject.get(getParent());
            parentShip = (Ship) this.thingObject;
            setElements();
            removedJob = this;
            new Thread(this).start();
        }
    }

    //definition of the method setElements()
    private void setElements() {
        panel = new JPanel();
        bar = new JProgressBar();
        bar.setStringPainted(true);
        stop.setMinimumSize(new Dimension(120, 25));
        stop.setMaximumSize(new Dimension(120, 25));
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);
        setLayout(groupLayout);
        setActionListeners();
    }

    //definition of the method setLayout()
    private void setLayout(GroupLayout groupLayout) {
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        JLabel jLabel = new JLabel(parentShip.getName(), SwingConstants.CENTER);
        jLabel.setMinimumSize(new Dimension(150, 25));
        jLabel.setMaximumSize(new Dimension(150, 25));
        groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
                .addComponent(bar)
                .addComponent(jLabel)
                .addComponent(stop)
                .addComponent(cancel));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(bar)
                .addComponent(jLabel)
                .addComponent(stop)
                .addComponent(cancel));
    }

    //definition of the method getPanel()
    public JPanel getPanel() {
        return panel;
    }

    //definition of the method setActionListeners()
    private void setActionListeners() {
        stop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setFlag();
            }
        });
        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setKillFlag();
            }
        });
    }

    //definition of the method setFlag()
    public void setFlag() {
        flag = !flag;
    }

    //definition of the method setKillFlag()
    public void setKillFlag() {
        killFlag = true;
        cancel.setBackground(Color.red);
        
    }

    //definition of the method showStatus()
    void showStatus(Status st) {
        status = st;
        switch (status) {
            case RUNNING:
                stop.setBackground(Color.blue);
                stop.setText("Running");
                break;
            case SUSPENDED:
                stop.setBackground(Color.yellow);
                stop.setText("Suspended");
                break;
            case WAITING:
                stop.setBackground(Color.orange);
                stop.setText("Waiting turn");
                break;
            case DONE:
                stop.setBackground(Color.green);
                stop.setText("Done");
                break;
            case CANCELLED:
            	stop.setBackground(Color.red);
            	stop.setText("Cancelled");
            	break;
        	default:
        		stop.setText("SOME ERROR OCCURED");
        }
    }

    public void run() {
        while (!World.getValue())
            waitTime(300);
        while (!parentShip.isShipDocked()) {
            waitTime(100);
        }
        while (!parentShip.processShip()) {
            waitTime(100);
        }
        ArrayList<Person> workers = null;
        if (requirements.size() == 0 || parentShip.requestPersonnel(requirements)) {
            if (requirements.size() != 0) {
                do {
                    waitTime(100);
                    workers = parentShip.requestWorkers(requirements, removedJob);
                } while (workers == null || workers.size() != requirements.size());
            }
            long time = System.currentTimeMillis();
            long startTime = time;
            long stopTime = time + 1000 * duration;
            double duration = stopTime - time;
            while (time < stopTime && !killFlag) {
                waitTime(100);
                if (flag) {
                    showStatus(Status.RUNNING);
                    time += 100;
                    bar.setValue((int) (((time - startTime) / duration) * 100));
                } else {
                    showStatus(Status.SUSPENDED);
                }
            }
            
            //If the job is cancelled, the progress bar will state so
            if(killFlag) {
            	bar.setValue(0);
            	bar.setString("CANCELLED");
            	showStatus(Status.CANCELLED);
            }
            else {
            	bar.setValue(100);
            	showStatus(Status.DONE);
            }
        } else {
            showStatus(Status.SUSPENDED);
        }
        if (workers != null && workers.size() > 0)
            parentShip.releaseWorkers(workers);
        parentShip.removeShip(removedJob);
    }

    private void waitTime(long l) {
        try {
            Thread.sleep(l);
        } catch (InterruptedException e) {
        }
    }

    public String toString() {
        return String.format("j:%7d:%15s:%5d", getIndex(), getName(), duration);
    }

}