// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar

import java.util.HashMap;
import java.util.Scanner;

public class Thing implements Comparable<Thing>{
    private String name;
    private int index, parent;
    Thing thingObject;

    //Constructors
    public Thing(){}
    public Thing(Scanner input){
        this.name = input.next();
        this.index = input.nextInt();
        this.parent = input.nextInt();
    }

    //Getters
    public String getName() {
        return name;
    }
    public int getIndex() {
        return index;
    }
    public int getParent() {
        return parent;
    }

    @SuppressWarnings("rawtypes")
    public void setThingObject(HashMap parentHashMap){
        if(parentHashMap.get(parent) != null)
            thingObject = (Thing) parentHashMap.get(parent);
    }

    @Override
    public int compareTo(Thing o) {
        return name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return "Thing{" +
                "name='" + name + '\'' +
                ", index=" + index +
                '}';
    }
}
