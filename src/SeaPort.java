// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class SeaPort extends Thing {
    ArrayList<Dock> docks = new ArrayList<Dock>();
    ArrayList<Ship> waitingShipsInQue = new ArrayList<Ship>();
    ArrayList<Ship> ships = new ArrayList<Ship>();
    ArrayList<Person> persons = new ArrayList<Person>();
    ArrayList<Job> rewaitingShipsInQuestList = new ArrayList<Job>();
    JPanel panel = new JPanel();
    JLabel resourceLabel = new JLabel("", SwingConstants.LEFT);
    JLabel rewaitingShipsInQuestLabel = new JLabel("", SwingConstants.LEFT);
    JProgressBar resourceProgress;
    //constructor
    public SeaPort(Scanner sc) {
        super(sc);
    }
    public void addDock(Dock dock) {
        docks.add(dock);
    }
    public void addShip(Ship ship) {
        ships.add(ship);
    }
    public void addShipToQue(Ship ship) {
        waitingShipsInQue.add(ship);
    }
    public void addPerson(Person person) {
        persons.add(person);
    }

    public void sortByWeight() {
        Collections.sort(waitingShipsInQue, new Comparator<Ship>() {
            @Override
            public int compare(Ship s1, Ship s2) {
                return (s1.getWidth() < s2.getWeight() ? -1 : (s1.getWeight() == s2.getWeight() ? 0 : 1));
            }
        });
    }
    public void sortByLength() {
        Collections.sort(waitingShipsInQue, new Comparator<Ship>() {
            @Override
            public int compare(Ship s1, Ship s2) {
                return (s1.getLength() < s2.getLength() ? -1 : (s1.getLength() == s2.getLength() ? 0 : 1));
            }
        });
    }
    public void sortByWidth() {
        Collections.sort(waitingShipsInQue, new Comparator<Ship>() {
            @Override
            public int compare(Ship s1, Ship s2) {
                return (s1.getWidth() < s2.getWidth() ? -1 : (s1.getWidth() == s2.getWidth() ? 0 : 1));
            }
        });
    }
    public void sortByDraft() {
        Collections.sort(waitingShipsInQue, new Comparator<Ship>() {
            @Override
            public int compare(Ship s1, Ship s2) {
                return (s1.getDraft() < s2.getDraft() ? -1 : (s1.getDraft() == s2.getDraft() ? 0 : 1));
            }
        });
    }
    public void sortAllListsByName() {
        Collections.sort(ships);
        Collections.sort(docks);
        Collections.sort(persons);
        Collections.sort(waitingShipsInQue);
        for (Ship ship : ships) {
            Collections.sort(ship.getShips());
        }
        for (Ship ship : waitingShipsInQue) {
            Collections.sort(ship.getShips());
        }
    }
    public Ship getShipFromQue() {
        synchronized (this) {
            if (waitingShipsInQue.size() > 0) {
                Ship ship = waitingShipsInQue.get(waitingShipsInQue.size() - 1);
                waitingShipsInQue.remove(ship);
                return ship;
            }
        }
        return null;
    }
    public String toString() {
        String result = "\n\nSeaPort: " + super.toString();
        for (Dock md : docks)
            result += "\n" + md.toString();
        result += "\n\n --- List of all ships in waitingShipsInQue:";
        for (Ship ms : waitingShipsInQue)
            result += "\n > " + ms.toString();
        result += "\n\n --- List of all ships:";
        for (Ship ms : ships)
            result += "\n > " + ms.toString();
        result += "\n\n --- List of all persons:";
        for (Person mp : persons)
            result += "\n > " + mp.toString();
        return result;
    }
    public synchronized void checkDocksAtSeaPort() {
        for (Dock dock : docks) {
            dock.checkDocksAtDock();
        }
    }
    public boolean requestPersonnel(ArrayList<String> requirements) {
        ArrayList<Person> check = new ArrayList<Person>();
        for (String requirement : requirements) {
            for (Person person : persons) {
                if (person.hasRequiredSkill(requirement) && !check.contains(person))
                    check.add(person);
                if (check.size() == requirements.size()) return true;
            }
        }
        return false;
    }
    public ArrayList<Person> requestWorkers(ArrayList<String> requirements, Job job) {
        ArrayList<Person> requiredWorkers = new ArrayList<Person>();
        for (String requirement : requirements) {
            for (Person person : persons) {
                if (person.hasRequiredSkill(requirement))
                    synchronized (this) {
                        requiredWorkers.add(person.hirePerson());
                    }
                if (requiredWorkers.size() == requirements.size()) {
                    broadcastUpdate();
                    if (rewaitingShipsInQuestList.contains(job)) rewaitingShipsInQuestList.remove(job);
                    return requiredWorkers;
                }
            }
        }
        if (!rewaitingShipsInQuestList.contains(job)) {
            rewaitingShipsInQuestList.add(job);
        }
        if (requiredWorkers.size() > 0)
            releaseWorkers(requiredWorkers);
        return null;
    }
    public void releaseWorkers(ArrayList<Person> workers) {
        for (Person worker : workers) {
            if (worker != null)
                synchronized (this) {
                    worker.release();
                }
        }
        broadcastUpdate();
    }
    private void broadcastUpdate() {
        int availableResources = getAvailableResources();
        resourceLabel.setText("Resources: " + availableResources);
        rewaitingShipsInQuestLabel.setText("RewaitingShipsInQuests: " + rewaitingShipsInQuestList.size());
        if (resourceProgress != null) {
            resourceProgress.setValue(availableResources);
            if (availableResources < Math.round(persons.size() * 0.2)) {
                resourceProgress.setForeground(Color.red);
            } else if (availableResources < Math.round(persons.size() * 0.6)) {
                resourceProgress.setForeground(Color.yellow);
            } else {
                resourceProgress.setForeground(Color.blue);
            }
        }
    }
    private int getAvailableResources() {
        int numOfAvailablePerson = 0;
        for (Person person : persons) {
            if (person.available)
                numOfAvailablePerson++;
        }
        return numOfAvailablePerson;
    }
    private void setElements() {
        panel = new JPanel();
        rewaitingShipsInQuestLabel.setMaximumSize(new Dimension(100, 20));
        rewaitingShipsInQuestLabel.setMaximumSize(new Dimension(100, 20));
        resourceLabel.setMinimumSize(new Dimension(100, 20));
        resourceLabel.setMinimumSize(new Dimension(100, 20));
        GroupLayout groupLayout = new GroupLayout(panel);
        panel.setLayout(groupLayout);
        setLayout(groupLayout);
    }
    private void setLayout(GroupLayout groupLayout) {
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        JLabel portLabel = new JLabel("Port: " + getName(), SwingConstants.LEFT);
        portLabel.setMaximumSize(new Dimension(120, 20));
        portLabel.setMinimumSize(new Dimension(120, 20));
        resourceProgress = new JProgressBar(0, persons.size());
        broadcastUpdate();
        groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
                .addComponent(portLabel)
                .addComponent(resourceProgress)
                .addComponent(resourceLabel)
                .addComponent(rewaitingShipsInQuestLabel)
        );
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(portLabel)
                .addComponent(resourceProgress)
                .addComponent(resourceLabel)
                .addComponent(rewaitingShipsInQuestLabel)
        );
    }
    public Component getContainerPanel() {
        setElements();
        return panel;
    }


}
