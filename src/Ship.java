// File: Dock.java
// Date: July 1, 2018
// Author: Sukhmeet Khalar

import java.util.ArrayList;
import java.util.Scanner;

public class Ship extends Thing {

    private PortTime arrivalTime, dockTime;
    private double weight, length, width, draft;
    private ArrayList < Job > ships = new ArrayList < Job > ();
    private final Object lock = new Object();
    private boolean shipInProcess;
    private Thing shipThing = thingObject;

    public Ship(Scanner key){
        super(key);
        if (key.hasNextDouble())
            weight = key.nextDouble();
        if (key.hasNextDouble())
            length = key.nextDouble();
        if (key.hasNextDouble())
            width = key.nextDouble();
        if (key.hasNextDouble())
            draft = key.nextDouble();
        dockTime = new PortTime();
    }


    public boolean isShipDocked(){
        if (thingObject instanceof SeaPort)
            ((SeaPort) thingObject).checkDocksAtSeaPort();
        else
            ((Dock) thingObject).checkDocksAtDock();
        return thingObject instanceof Dock
                && ((Dock) thingObject).getShip() == this;
    }


    public boolean processShip(){
        boolean process = false;
        synchronized(lock)
        {
            if (!shipInProcess)
            {
                shipInProcess = true;
                process = true;
            }
        }
        return process;
    }


    public void removeShip(Job ship){
        synchronized(lock)
        {
            shipInProcess = false;
        }
        if (ships.size() > 0 && ships.contains(ship))
            ships.remove(ship);
        if (ships.size() == 0)
        {
            ((Dock) thingObject).leaveShipFromDock();
        }
    }

    boolean requestPersonnel(ArrayList < String > requirements){
        return ((Dock) thingObject).requestPersonnel(requirements);
    }

    public ArrayList < Person > requestWorkers(ArrayList < String > requirements,
                                               Job ship){
        return ((Dock) thingObject).requestWorkers(requirements, ship);
    }

    public void releaseWorkers(ArrayList < Person > workers)
    {
        ((Dock) thingObject).releaseWorkers(workers);
    }


    //Getters
    public PortTime getArrivalTime() {
        return arrivalTime;
    }
    public PortTime getDockTime() {
        return dockTime;
    }
    public double getWeight() {
        return weight;
    }
    public double getLength() {
        return length;
    }
    public double getWidth() {
        return width;
    }
    public double getDraft() {
        return draft;
    }
    public ArrayList<Job> getShips() {
        return ships;
    }
    public boolean isShipInProcess() {
        return shipInProcess;
    }
	public Thing getShipThing() {
		return shipThing;
	}
    

    //toString
    public String toString() {
        String returns = (this instanceof PassengerShip ? "Passenger " : "Cargo ");
        returns += "Ship: " + super.toString();
        if (ships.size() == 0)
            return returns;
        for (Job job: ships)
            returns += "\n - " + job.toString();
        return returns;
    }
}